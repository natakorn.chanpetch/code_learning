{{ config(materialized="table") }}

with source as (
    select *
    from {{ ref('customers_snapshot') }}
), 
final as (
    select customer_id,
        zipcode,
        city,
        state_code,
        cast(datetime_created as TIMESTAMP) as datetime_created,
        cast(datetime_updated as TIMESTAMP) as datetime_updated,
        dbt_valid_from,
        dbt_valid_to
    from source
)

select * from final