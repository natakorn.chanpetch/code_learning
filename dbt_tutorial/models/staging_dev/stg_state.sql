{{ config(materialized="table") }}

with raw_state as (
    select * from  {{ ref('state') }}
),
final as (
    select  cast(state_id as int) as state_id
            , cast(state_code as varchar(2)) as state_code
            , cast(state_name as varchar(30)) as state_name
    from raw_state 
)

select * from final








    
