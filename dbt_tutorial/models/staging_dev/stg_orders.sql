{{ config(materialized="table") }}
with raw_orders as (
    select * from {{ ref('orders') }}
), 

final as (
    select   order_id
            , customer_id
            , order_status
            , cast(order_purchase_timestamp as timestamp) 
            , cast(order_approved_at as timestamp)
            , cast(order_delivered_carrier_date as timestamp) 
            , cast(order_delivered_customer_date as timestamp)
            , cast(order_estimated_delivery_date as timestamp)
    from raw_orders
)

select * from final